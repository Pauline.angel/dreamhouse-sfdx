@isTest
public class TestVerifyDate {
                                     
    @isTest static void checkDates_date2(){
        Date date1 = Date.newInstance(2020, 3, 1);
        Date date2 = Date.newInstance(2020, 3, 16);

        System.assertEquals(date2, VerifyDate.CheckDates(date1, date2));
    }
    
        @isTest static void checkDates_endofmonth(){
        
        Date date1 = Date.newInstance(2020, 3, 1);
        Date date3= Date.newInstance(2020, 4, 26);
        
        Integer totalDays = Date.daysInMonth(date1.year(), date1.month());
		Date lastDay = Date.newInstance(date1.year(), date1.month(), totalDays);
        System.assertEquals(lastDay, VerifyDate.CheckDates(date1, date3));
    }

}