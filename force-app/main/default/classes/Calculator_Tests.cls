@isTest
public class Calculator_Tests {
     @testSetup
    static void setupData(){
        
    }
    
    
    @isTest static void testPositiveAddition() {
        Test.startTest();
        
        Integer result = Calculator.addition(2, 10);
        
        Test.stopTest();
        System.assertEquals(12, result);
    }
    
    @isTest static void testPositiveSubtraction() {
        Test.startTest();
        
        Integer result = Calculator.subtraction(10, 2);
        
        Test.stopTest();
        System.assertEquals(8, result);
    }
    
    @isTest static void testPositiveMultiply() {
        Test.startTest();
        
        Integer result = Calculator.multiply(2, 10);
        
        Test.stopTest();
        System.assertEquals(20, result);
    }
    
    @isTest static void testNegativeMultiply() {
        Test.startTest();
        try {
        Calculator.multiply(0, 10);
        } catch (Calculator.CalculatorException e) {
        Test.stopTest();
        System.assertEquals('It doesn\'t make sense to multiply by zero', e.getMessage(),'caught the right exception');
        }
    }
    
    @isTest static void testPositiveDivide() {
        Test.startTest();
        
        Decimal result = Calculator.divide(10, 2);
        
        Test.stopTest();
        System.assertEquals(5, result);
    }
    
    @isTest static void testNegativeDivide_test1() {
        Test.startTest();
        try {
        Calculator.divide(10, 0);
        } catch (Calculator.CalculatorException e) {
        Test.stopTest();
        System.assertEquals('you still can\'t divide by zero', e.getMessage(),'caught the right exception');
        }
        }
    
    @isTest static void testNegativeDivide_test2() {
        Decimal returnValue = -10/2;
        Test.startTest();
        try {
        Calculator.divide(-10, 2);
        } catch (Calculator.CalculatorException e) {
        Test.stopTest();
        System.assertEquals('Division returned a negative value.' + returnValue, e.getMessage(),'caught the right exception');
        }
        
    }
    
    
}