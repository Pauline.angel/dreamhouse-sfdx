@isTest
private class ExternalSearch_Tests {
    @isTest static void test_method_one() {
        HttpMockFactory mock = new HttpMockFactory(200, 'OK', 'I found it!', new Map<String,String>());
        Test.setMock(HttpCalloutMock.class, mock);
        String result;
        Test.startTest();
        result = ExternalSearch.googleIt('epic search');
        Test.stopTest();
        system.assertEquals('I found it!', result);
    }
    
    
    @isTest static void test_method_two() {
        HttpMockFactory mock = new HttpMockFactory(500, 'OK', 'Internal Server Error', new Map<String,String>());
        Test.setMock(HttpCalloutMock.class, mock);
        String result;
        Test.startTest();
        
        try
        {
            ExternalSearch.googleIt('500');
        }
        catch (ExternalSearch.ExternalSearchException e)
        {
            Test.stopTest();
            system.assertEquals('Did not recieve a 200 status code: 500', e.getMessage(), e.getMessage());
        }
    }
}