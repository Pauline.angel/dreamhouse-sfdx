@isTest
public class TestRestrictContactByName {

        @isTest static void TestRestrictContactByNameWithValidName(){
            String lastName = 'INVALIDNAME';
            Test.startTest();
            Contact co = new Contact(LastName=lastName);
            
           	Database.SaveResult result = Database.insert(co, false);
            Test.stopTest();
            System.assert(!result.isSuccess());
            System.assert(result.getErrors().size() > 0);
            System.assertEquals('The Last Name "'+co.lastName+'" is not allowed for DML',
                             result.getErrors()[0].getMessage());
            
        }
        
        @isTest static void TestRestrictContactByNameWithInvalidName(){
            String lastName = 'validname';
            Test.startTest();
            Contact co = new Contact(LastName=lastName);
            
           	Database.SaveResult result = Database.insert(co, false);
            Test.stopTest();
            System.assert(result.isSuccess());
            
            
        }
}